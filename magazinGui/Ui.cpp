#include "Ui.h"
#include <iostream>
#include <string>
#include <vector>
#include "Item.h"
void Ui::run() {
	this->printMenu();
	this->adaugareRandom();
	int comanda = 0;
	while (true) {
		std::cout << std::endl;
		std::cout << "Comanda: ";
		std::cin >> comanda;
		std::cout << std::endl;
		if (comanda == 0) {
			std::cout << "O zi buna!";
			break;
		}
		switch (comanda) {
		case 1:
			adaugareProdus();
			break;
		case 2:
			stergereProdus();
			break;
		case 3:
			modificareNume();
			break;
		case 4:
			modificareTip();
			break;
		case 5:
			modificarePret();
			break;
		case 6:
			modificareProducator();
			break;
		case 7:
			afisareProduse();
			break;
		case 8:
			cautareProdus();
			break;
		case 9:
			filtrarePretMaiMic();
			break;
		case 10:
			filtrarePretMaiMare();
			break;
		case 11:
			filtrareNume();
			break;
		case 12:
			filtrareProducator();
			break;
		case 13:
			sortareNume();
			break;
		case 14:
			sortarePret();
			break;
		case 15:
			sortareNumeSiTip();
			break;
		default:
			break;
		}
	}
}

void Ui::printMenu() {
	std::cout << "1. Adaugare produs." << std::endl;
	std::cout << "2. Stergere produs." << std::endl;
	std::cout << "3. Modificare nume." << std::endl;
	std::cout << "4. Modificare tip." << std::endl;
	std::cout << "5. Modificare pret." << std::endl;
	std::cout << "6. Modificare producator." << std::endl;
	std::cout << "7. Afisare produse." << std::endl;
	std::cout << "8. Cautare produs." << std::endl;
	std::cout << "9. Filtrare dupa pret mai mic." << std::endl;
	std::cout << "10. Filtrare dupa pret mai mare." << std::endl;
	std::cout << "11. Filtrare dupa nume." << std::endl;
	std::cout << "12. Filtrare dupa producator." << std::endl;
	std::cout << "13. Sortare dupa nume." << std::endl;
	std::cout << "14. Sortare dupa pret." << std::endl;
	std::cout << "15. Sortare dupa nume + tip." << std::endl;
	std::cout << "0. Exit." << std::endl;

}

void Ui::adaugareProdus() {
	std::string nume, tip, producator;
	double pret;
	std::cout << "Nume: ";
	std::cin >> nume;
	std::cout << "Tip: ";
	std::cin >> tip;
	std::cout << "Pret: ";
	std::cin >> pret;
	std::cout << "Producator: ";
	std::cin >> producator;
	service.add(nume, tip, pret, producator);
}

void Ui::stergereProdus() {
	std::string nume, producator;
	std::cout << "Nume: ";
	std::cin >> nume;
	std::cout << "Producator: ";
	std::cin >> producator;
	service.remove(nume, producator);
}

void Ui::modificareNume() {
	std::string nume, newName, producator;
	std::cout << "Nume: ";
	std::cin >> nume;
	std::cout << "Producator: ";
	std::cin >> producator;
	std::cout << "Noul nume: ";
	std::cin >> newName;
	service.modifyNume(nume, producator, newName);
}

void Ui::modificareTip() {
	std::string nume, newTip, producator;
	std::cout << "Nume: ";
	std::cin >> nume;
	std::cout << "Producator: ";
	std::cin >> producator;
	std::cout << "Noul tip: ";
	std::cin >> newTip;
	service.modifyTip(nume, producator, newTip);
}

void Ui::modificarePret() {
	std::string nume, producator;
	double newPret;
	std::cout << "Nume: ";
	std::cin >> nume;
	std::cout << "Producator: ";
	std::cin >> producator;
	std::cout << "Noul pret: ";
	std::cin >> newPret;
	service.modifyPret(nume, producator, newPret);
}

void Ui::modificareProducator() {
	std::string nume, newProducator, producator;
	std::cout << "Nume: ";
	std::cin >> nume;
	std::cout << "Producator: ";
	std::cin >> producator;
	std::cout << "Noul tip: ";
	std::cin >> newProducator;
	service.modifyProducator(nume, producator, newProducator);
}

void Ui::afisareProduse() {
	std::vector<Item> vector = service.getAll();
	std::string text;
	for (int i = 0; i < (int)vector.size(); i++) {
		std::cout << "Nume: " << vector[i].getNume() << std::endl;
		std::cout << "Tip: " << vector[i].getTip() << std::endl;
		std::cout << "Pret: " << vector[i].getPret() << std::endl;
		std::cout << "Producator: " << vector[i].getProducator() << std::endl;
		std::cout << std::endl;
	}
}

void Ui::cautareProdus() {
	std::string nume, producator;
	std::cout << "Nume: ";
	std::cin >> nume;
	std::cout << "Producator: ";
	std::cin >> producator;
	Item item = service.getItem(nume, producator);
	std::cout << "Nume: " << item.getNume() << std::endl;
	std::cout << "Tip: " << item.getTip() << std::endl;
	std::cout << "Pret: " << item.getPret() << std::endl;
	std::cout << "Producator: " << item.getProducator() << std::endl;
	std::cout << std::endl;
}

void Ui::filtrarePretMaiMic() {
	double pret;
	std::cout << "Pret: ";
	std::cin >> pret;
	std::vector<Item> vector = service.filtrarePretMaiMic(pret);
	for (int i = 0; i < (int)vector.size(); i++) {
		std::cout << "Nume: " << vector[i].getNume() << std::endl;
		std::cout << "Tip: " << vector[i].getTip() << std::endl;
		std::cout << "Pret: " << vector[i].getPret() << std::endl;
		std::cout << "Producator: " << vector[i].getProducator() << std::endl;
		std::cout << std::endl;
	}
}

void Ui::filtrarePretMaiMare() {
	double pret;
	std::cout << "Pret: ";
	std::cin >> pret;
	std::vector<Item> vector = service.filtrarePretMaiMare(pret);
	for (int i = 0; i < (int)vector.size(); i++) {
		std::cout << "Nume: " << vector[i].getNume() << std::endl;
		std::cout << "Tip: " << vector[i].getTip() << std::endl;
		std::cout << "Pret: " << vector[i].getPret() << std::endl;
		std::cout << "Producator: " << vector[i].getProducator() << std::endl;
		std::cout << std::endl;
	}
}

void Ui::filtrareNume() {
	std::string nume;
	std::vector<Item> vector = service.filtrareNume(nume);
	for (int i = 0; i < (int)vector.size(); i++) {
		std::cout << "Nume: " << vector[i].getNume() << std::endl;
		std::cout << "Tip: " << vector[i].getTip() << std::endl;
		std::cout << "Pret: " << vector[i].getPret() << std::endl;
		std::cout << "Producator: " << vector[i].getProducator() << std::endl;
		std::cout << std::endl;
	}
}

void Ui::filtrareProducator() {
	std::string producator;
	std::vector<Item> vector = service.filtrareProducator(producator);
	for (int i = 0; i < (int)vector.size(); i++) {
		std::cout << "Nume: " << vector[i].getNume() << std::endl;
		std::cout << "Tip: " << vector[i].getTip() << std::endl;
		std::cout << "Pret: " << vector[i].getPret() << std::endl;
		std::cout << "Producator: " << vector[i].getProducator() << std::endl;
		std::cout << std::endl;
	}
}

void Ui::sortareNume() {
	std::vector<Item> vector = service.sortareNume();
	for (int i = 0; i < (int)vector.size(); i++) {
		std::cout << "Nume: " << vector[i].getNume() << std::endl;
		std::cout << "Tip: " << vector[i].getTip() << std::endl;
		std::cout << "Pret: " << vector[i].getPret() << std::endl;
		std::cout << "Producator: " << vector[i].getProducator() << std::endl;
		std::cout << std::endl;
	}
}

void Ui::sortarePret() {
	std::vector<Item> vector = service.sortarePret();
	for (int i = 0; i < (int)vector.size(); i++) {
		std::cout << "Nume: " << vector[i].getNume() << std::endl;
		std::cout << "Tip: " << vector[i].getTip() << std::endl;
		std::cout << "Pret: " << vector[i].getPret() << std::endl;
		std::cout << "Producator: " << vector[i].getProducator() << std::endl;
		std::cout << std::endl;
	}
}

void Ui::sortareNumeSiTip() {
	std::vector<Item> vector = service.sortareNumeSiTip();
	for (int i = 0; i < (int)vector.size(); i++) {
		std::cout << "Nume: " << vector[i].getNume() << std::endl;
		std::cout << "Tip: " << vector[i].getTip() << std::endl;
		std::cout << "Pret: " << vector[i].getPret() << std::endl;
		std::cout << "Producator: " << vector[i].getProducator() << std::endl;
		std::cout << std::endl;
	}
}

void Ui::adaugareRandom() {
	service.add("Capsuni", "Fructe", 10, "Kaufland");
	service.add("Castraveti", "Legume", 10, "Kaufland");
	service.add("Rosii", "Legume", 54, "Profi");
	service.add("Ceapa", "Legume", 31, "Kaufland");
	service.add("Banane", "Fructe", 20, "Profi");
	service.add("Nectarine", "Fructe", 30, "Kaufland");
	service.add("Piersici", "Fructe", 5, "Profi");
	service.add("caise", "Fructe", 23, "Kaufland");
	service.add("Usturoi", "Legume", 1, "Profi");
	service.add("Ardei", "Legume", 44, "Kaufland");
}
