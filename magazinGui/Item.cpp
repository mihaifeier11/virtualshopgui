#include "Item.h"
#include <string.h>
#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <sstream>

Item::Item(const std::string nume, const std::string tip, const double pret, const std::string producator) : nume{ nume }, tip{ tip }, pret{ pret }, producator{ producator } {
}


std::string Item::getNume() const {

	return this->nume;
}

std::string Item::getTip() const {
	return this->tip;
}

double Item::getPret() const {
	return this->pret;
}

std::string Item::getProducator() const {
	return this->producator;
}

void Item::modifyNume(const std::string newNume) {
	this->nume = newNume;
}

void Item::modifyTip(const std::string newTip) {
	this->tip = newTip;
}

void Item::modifyPret(const double newPret) {
	this->pret = newPret;
}

void Item::modifyProducator(const std::string newProducator) {
	this->producator = newProducator;
}

std::string Item::getItem() const {
	return this->nume + "," + this->tip + "," + std::to_string(this->pret) + "," + this->producator;
}


bool Item::operator==(const Item & item) {
	if (getNume() == item.getNume() && getProducator() == item.getProducator()) {
		return true;
	}
	return false;
}

void Item::operator=(const Item & item2) {
	this->modifyNume(item2.getNume());
	this->modifyTip(item2.getTip());
	this->modifyPret(item2.getPret());
	this->modifyProducator(item2.getProducator());
}

std::vector<std::string> tokenize(const std::string& str, char delimiter)
{
	std::vector <std::string> result;
	std::stringstream ss(str);
	std::string token;
	while (getline(ss, token, delimiter))
		result.push_back(token);

	return result;
}

std::istream & operator>>(std::istream & is, Item & item) {
	std::string line;
	getline(is, line);

	std::vector<std::string> tokens = tokenize(line, ',');
	if (tokens.size() != 4)
		return is;

	item.modifyNume(tokens[0]);
	item.modifyTip(tokens[1]);
	item.modifyPret(stoi(tokens[2]));
	item.modifyProducator(tokens[3]);

	return is;
}

std::ostream & operator<<(std::ostream & os, Item & item) {
	os << item.getNume() << "," << item.getTip() << "," << std::to_string(item.getPret()) << "," << item.getProducator() << std::endl;
	return os;
}

