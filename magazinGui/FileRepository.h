#pragma once
#include "Repository.h"
#include <fstream>
#include <string.h>
#include <iostream>
template<class T>

class FileRepository {
private:
	std::string fileName = "in.txt";
	std::vector<T> items;
	
public:
	FileRepository() = default;
	~FileRepository() = default;

	void open() {
		std::ifstream in(fileName);
		T item;
		while (in >> item) {
			items.push_back(item);
		}
		
		in.close();
	}
	void write() {
		std::ofstream out(fileName);
		for (int i = 0; i < (int)items.size(); i++) {
			out << items[i];
		}
		out.close();
	}

	void add(const T item) {
		this->items.push_back(item);
		write();
	}

	void remove(const int position) {
		this->items.erase(items.begin() + position);
		write();
	}

	const T getItem(const int position) const {
		return this->items[position];
	}

	const int getSize() const {
		return this->items.size();
	}

	int find(const T item) {
		int exist = -1;
		for (int i = 0; i < (int)items.size(); i++) {
			if (this->items[i] == item)
				exist = i;
		}
		return exist;
	}

	void update(const T item, const int position) {
		this->items[position] = item;
		write();
	}

	std::vector<T> getAll() {
		return items;
	}

	std::vector<T> getRepository() {
		return items;
	}
};
