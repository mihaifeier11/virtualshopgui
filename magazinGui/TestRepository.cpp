#include "TestRepository.h"
#include "Repository.h"
#include "Item.h"
#include <iostream>
#include <assert.h>


TestRepository::TestRepository() {
}


TestRepository::~TestRepository() {
}

void TestRepository::testAll() {
	this->testAdd();
	this->testGetItem();
	this->testGetSize();
	this->testRemove();
	this->testFind();
	this->testUpdate();
}

void TestRepository::testAdd() {
	Item item("Capsuni", "Fructe", 232.2, "Profi");
	Repository<Item> repository;
	repository.add(item);
	assert(repository.getItem(0).getItem() == "Capsuni,Fructe,232.200000,Profi");
}

void TestRepository::testGetItem() {
	Item item("Capsuni", "Fructe", 232.2, "Profi");
	Repository<Item> repository;
	repository.add(item);
	assert(repository.getItem(0).getItem() == "Capsuni,Fructe,232.200000,Profi");
}

void TestRepository::testRemove() {
	Item item("Capsuni", "Fructe", 232.2, "Profi");
	Repository<Item> repository;
	repository.add(item);
	repository.remove(0);
	assert(repository.getSize() == 0);
}

void TestRepository::testGetSize() {
	Item item("Capsuni", "Fructe", 232.2, "Profi");
	Repository<Item> repository;
	assert(repository.getSize() == 0);
	repository.add(item);
	assert(repository.getSize() == 1);
}

void TestRepository::testFind() {
	Item item("Capsuni", "Fructe", 232.2, "Profi");
	Repository<Item> repository;
	assert(repository.find(item) == -1);
	repository.add(item);
	assert(repository.find(item) == 0);
}

void TestRepository::testUpdate() {
	Item item("Capsuni", "Fructe", 232.2, "Profi");
	Item item2("Banane", "Fructe", 232.2, "Profi");
	Repository<Item> repository;
	repository.add(item);
	repository.update(item2, 0);
	assert(repository.find(item2) == 0);
}


