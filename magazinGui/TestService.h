#pragma once
class TestService {
public:
	TestService() = default;
	~TestService() = default;
	void testAdd();
	void testAll();
	void testRemove();
	void testModifyNume();
	void testModifyTip();
	void testModifyPret();
	void testModifyProducator();
	void testGetItem();
	void testFiltrarePretMaiMic();
	void testFiltrarePretMaiMare();
	void testFiltrareNume();
	void testFiltareProducator();
	void testSortareNume();
	void testSortarePret();
	void testSortareNumeSiTip();
	void testGetAll();
};

