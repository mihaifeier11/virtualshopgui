#include "magazinGui.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
	Service service;
	QApplication a(argc, argv);
	magazinGui w{ service };
	w.show();
	a.exec();
	return 0;
}
