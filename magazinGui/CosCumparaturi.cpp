#include "CosCumparaturi.h"


void CosCumparaturi::total(double pret) {
	totalPret += pret;
}

void CosCumparaturi::adauga(Item item) {
	cos.push_back(item);
	total(item.getPret());
}

double CosCumparaturi::getTotal() {
	return totalPret;
}

void CosCumparaturi::golireCos() {
	cos.clear();
	totalPret = 0;
}
