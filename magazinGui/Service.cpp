#include "Service.h"
#include "Item.h"
#include <assert.h>
#include "Undo.h"


void Service::add(std::string nume, std::string tip, double pret, std::string producator) {

	Item item(nume, tip, pret, producator);
	if (repository.find(item) != -1) {
		throw ServiceException("Elementul exista deja.");
	}
	this->repository.add(item);
	undo.push_back(std::make_unique<AddUndo>(repository, item));
	
}

void Service::remove(std::string nume, std::string producator) {
	Item item(nume, "", 0, producator);
	if (repository.find(item) == -1) {
		throw ServiceException("Elementul nu exista.");
	}
	int position = repository.find(item);
	repository.remove(position);
	undo.push_back(std::make_unique<RemoveUndo>(repository, item));
}

void Service::modifyNume(std::string nume, std::string producator, std::string newNume) {
	Item item(nume, "", 0, producator);
	if (repository.find(item) == -1) {
		throw ServiceException("Elementul nu exista.");
	}
	int position = repository.find(item);
	item = repository.getItem(position);
	item.modifyNume(newNume);
	repository.update(item, position);
	undo.push_back(std::make_unique<UpdateUndo>(repository, item));
}

void Service::modifyTip(std::string nume, std::string producator, std::string newTip) {
	Item item(nume, "", 0, producator);
	if (repository.find(item) == -1) {
		throw ServiceException("Elementul nu exista.");
	}
	int position = repository.find(item);
	item = repository.getItem(position);
	item.modifyTip(newTip);
	repository.update(item, position);
	undo.push_back(std::make_unique<UpdateUndo>(repository, item));
}

void Service::modifyPret(std::string nume, std::string producator, double newPret) {
	Item item(nume, "", 0, producator);
	if (repository.find(item) == -1) {
		throw ServiceException("Elementul nu exista.");
	}
	int position = repository.find(item);
	item = repository.getItem(position);
	item.modifyPret(newPret);
	repository.update(item, position);
	undo.push_back(std::make_unique<UpdateUndo>(repository, item));
}

void Service::modifyProducator(std::string nume, std::string producator, std::string newProducator) {
	Item item(nume, "", 0, producator);
	if (repository.find(item) == -1) {
		throw ServiceException("Elementul nu exista.");
	}
	int position = repository.find(item);
	item = repository.getItem(position);
	item.modifyProducator(newProducator);
	repository.update(item, position);
	undo.push_back(std::make_unique<UpdateUndo>(repository, item));
}

Item Service::getItem(std::string nume, std::string producator) {
	Item item(nume, "", 0, producator);
	if (repository.find(item) == -1) {
		throw ServiceException("Elementul nu exista.");
	}
	int position = repository.find(item);
	item = repository.getItem(position);
	return item;
}

std::vector<Item> Service::filtrarePretMaiMic(double pret) {
	std::vector<Item> repoItems = repository.getRepository();
	std::vector<Item> repoFiltrat;
	for (int i = 0; i <(int)repoItems.size(); i++) {
		if(repoItems[i].getPret() < pret){
			repoFiltrat.push_back(repoItems[i]);
		}
	}
	if (repoFiltrat.empty()) {
		throw ServiceException("Nu exista elemente pentru filtrare.");
	}
	return repoFiltrat;
	
}

std::vector<Item> Service::filtrarePretMaiMare(double pret)
{
	std::vector<Item> repoItems = repository.getRepository();
	std::vector<Item> repoFiltrat;
	for (int i = 0; i < (int)repoItems.size(); i++) {
		if (repoItems[i].getPret() > pret) {
			repoFiltrat.push_back(repoItems[i]);
		}
	}
	if (repoFiltrat.empty()) {
		throw ServiceException("Nu exista elemente pentru filtrare.");
	}
	return repoFiltrat;
}

std::vector<Item> Service::filtrareNume(std::string nume) {
	std::vector<Item> repoItems = repository.getRepository();
	std::vector<Item> repoFiltrat;
	for (int i = 0; i < (int)repoItems.size(); i++) {
		if (repoItems[i].getNume() == nume) {
			repoFiltrat.push_back(repoItems[i]);
		}
	}
	if (repoFiltrat.empty()) {
		throw ServiceException("Nu exista elemente pentru filtrare.");
	}
	return repoFiltrat;
}

std::vector<Item> Service::filtrareProducator(std::string producator) {
	std::vector<Item> repoItems = repository.getRepository();
	std::vector<Item> repoFiltrat;
	for (int i = 0; i < (int)repoItems.size(); i++) {
		if (repoItems[i].getProducator() == producator) {
			repoFiltrat.push_back(repoItems[i]);
		}
	}
	if (repoFiltrat.empty()) {
		throw ServiceException("Nu exista elemente pentru filtrare.");
	}
	return repoFiltrat;
}

std::vector<Item> Service::sortareNume() {
	std::vector<Item> repoSortat = repository.getRepository();
	for (int i = 0; i < (int)repoSortat.size(); i++) {
		for (int j = i; j < (int)repoSortat.size(); j++){
			if (repoSortat[i].getNume() > repoSortat[j].getNume()) {
				std::swap(repoSortat[i], repoSortat[j]);
			}
		
		}
	}

	return repoSortat;
}

std::vector<Item> Service::sortarePret() {
	std::vector<Item> repoSortat = repository.getRepository();
	for (int i = 0; i < (int)repoSortat.size(); i++) {
		for (int j = i; j < (int)repoSortat.size(); j++) {
			if (repoSortat[i].getPret() > repoSortat[j].getPret()) {
				std::swap(repoSortat[i], repoSortat[j]);
			}

		}
	}

	return repoSortat;
}

std::vector<Item> Service::sortareNumeSiTip() {
	std::vector<Item> repoSortat = repository.getRepository();
	for (int i = 0; i < (int)repoSortat.size(); i++) {
		for (int j = i; j < (int)repoSortat.size(); j++) {
			if (repoSortat[i].getNume() + repoSortat[i].getTip() > repoSortat[j].getNume() + repoSortat[j].getTip()) {
				std::swap(repoSortat[i], repoSortat[j]);
			}

		}
	}

	return repoSortat;
}

std::vector<Item> Service::getAll() {
	return repository.getRepository();
}

void Service::doUndo() {
	if (!undo.empty()) {
		undo.back()->doUndo();
		undo.pop_back();
	}
	else {
		throw UndoException{ "Nu se mai pot anula operatii" };
	}
}

FileRepository<Item> Service::getRepository() {
	return repository;
}





