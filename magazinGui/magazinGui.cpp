#include "magazinGui.h"
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMessageBox>
#include <qwidget.h>
#include <qlistwidget.h>
#include <qpushbutton.h>
#include <qlineedit.h>
#include <qboxlayout.h>
#include <vector>
#include <qlabel.h>
#include "Item.h"

void magazinGui::initGUICmps() {
	QHBoxLayout * layout = new QHBoxLayout;
	setLayout(layout);
	QVBoxLayout* vLayout = new QVBoxLayout;
	list = new QListWidget;
	vLayout->addWidget(list);

	QWidget* widBtnDreapta = new QWidget;
	QHBoxLayout* layoutBtn = new QHBoxLayout;
	widBtnDreapta->setLayout(layoutBtn);

	QWidget* wDetalii = new QWidget;
	QFormLayout* formDetalii = new QFormLayout;
	wDetalii->setLayout(formDetalii);
	
	txtNume = new QLineEdit;
	formDetalii->addRow(new QLabel("Nume: "), txtNume);

	txtTip = new QLineEdit;
	formDetalii->addRow(new QLabel("Tip: "), txtTip);

	txtPret = new QLineEdit;
	formDetalii->addRow(new QLabel("Pret: "), txtPret);

	txtProducator = new QLineEdit;
	formDetalii->addRow(new QLabel("Producator: "), txtProducator);

	btnAdauga = new QPushButton("Adauga");
	btnSterge = new QPushButton("Sterge");

	formDetalii->addWidget(btnAdauga);
	formDetalii->addWidget(btnSterge);

	layout->addLayout(vLayout);
	layout->addWidget(wDetalii);
}

void magazinGui::connectSignalsSlots() {
	QObject::connect(list, &QListWidget::itemSelectionChanged, [&]() {
		std::string s = list->selectedItems()[0]->text().toStdString();
		Item item = service.getItem(s, "Kaufland");
		txtNume->setText(QString::fromStdString(item.getNume()));
	});
}

void magazinGui::reloadList(const std::vector<Item>& items) {
	list->clear();

	for (int i = 0; i < items.size(); i++) {
		QListWidgetItem* item = new QListWidgetItem(QString::fromStdString(items[i].getNume()), list);
		item->setData(Qt::UserRole, QString::fromStdString(items[i].getProducator()));
		list->addItem(item);
	}
}
