#pragma once

#include <QtWidgets/QMainWindow>
#include <qwidget.h>
#include <qlistwidget.h>
#include <qpushbutton.h>
#include <qlineedit.h>
#include <qboxlayout.h>
#include <vector>
#include <qlabel.h>
#include "Item.h"
#include "Service.h"

class magazinGui : public QWidget {
public:
	magazinGui(Service& service) : service{ service } {
		service.open();
		initGUICmps();
		connectSignalsSlots();
		reloadList(service.getAll());
	}

private:
	Service & service;
	QListWidget* list;
	QPushButton* btnAdauga;
	QPushButton* btnSterge;
	QLineEdit* txtNume;
	QLineEdit* txtTip;
	QLineEdit* txtPret;
	QLineEdit* txtProducator;
	void initGUICmps();
	void connectSignalsSlots();
	void reloadList(const std::vector<Item> &items);
};
