/********************************************************************************
** Form generated from reading UI file 'magazinGui.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAGAZINGUI_H
#define UI_MAGAZINGUI_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_magazinGuiClass
{
public:
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QWidget *centralWidget;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *magazinGuiClass)
    {
        if (magazinGuiClass->objectName().isEmpty())
            magazinGuiClass->setObjectName(QStringLiteral("magazinGuiClass"));
        magazinGuiClass->resize(600, 400);
        menuBar = new QMenuBar(magazinGuiClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        magazinGuiClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(magazinGuiClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        magazinGuiClass->addToolBar(mainToolBar);
        centralWidget = new QWidget(magazinGuiClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        magazinGuiClass->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(magazinGuiClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        magazinGuiClass->setStatusBar(statusBar);

        retranslateUi(magazinGuiClass);

        QMetaObject::connectSlotsByName(magazinGuiClass);
    } // setupUi

    void retranslateUi(QMainWindow *magazinGuiClass)
    {
        magazinGuiClass->setWindowTitle(QApplication::translate("magazinGuiClass", "magazinGui", nullptr));
    } // retranslateUi

};

namespace Ui {
    class magazinGuiClass: public Ui_magazinGuiClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAGAZINGUI_H
