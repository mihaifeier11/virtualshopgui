#include "TestItem.h"
#include <string>
#include "Item.h"
#include <assert.h>
#include <iostream>

TestItem::TestItem() {

}


TestItem::~TestItem() {

}

void TestItem::testConstructor() {
	Item item("Capsuni", "Fructe", 232.2, "Profi");
	assert(item.getTip() == "Fructe");
	assert(item.getNume() == "Capsuni");
	assert(item.getPret() == 232.2);
	assert(item.getProducator() == "Profi");	
}

void TestItem::testAll() {
	this->testConstructor();

	this->testGetNume();
	this->testGetProducator();
	this->testGetTip();
	this->testGetPret();

	this->testModifyNume();
	this->testModifyPret();
	this->testModifyProducator();
	this->testModifyTip();

	this->testGetItem();
	
	this->testIsEqual();
	this->testAsigned();
}

void TestItem::testGetTip() {
	Item item("Capsuni", "Fructe", 232.2, "Profi");
	assert(item.getTip() == "Fructe");
}

void TestItem::testGetNume() {
	Item item("Capsuni", "Fructe", 232.2, "Profi");
	assert(item.getNume() == "Capsuni");
}

void TestItem::testGetPret() {
	Item item("Capsuni", "Fructe", 232.2, "Profi");
	assert(item.getPret() == 232.2);
}

void TestItem::testGetProducator() {
	Item item("Capsuni", "Fructe", 232.2, "Profi");
	assert(item.getProducator() == "Profi");
}

void TestItem::testModifyNume() {
	Item item("Capsuni", "Fructe", 232.2, "Profi");
	item.modifyNume("Banane");
	assert(item.getNume() == "Banane");
}

void TestItem::testModifyTip() {
	Item item("Capsuni", "Fructe", 232.2, "Profi");
	item.modifyTip("Legume");
	assert(item.getTip() == "Legume");
}

void TestItem::testModifyPret() {
	Item item("Capsuni", "Fructe", 232.2, "Profi");
	item.modifyPret(120);
	assert(item.getPret() == 120);
}

void TestItem::testModifyProducator() {
	Item item("Capsuni", "Fructe", 232.2, "Profi");
	item.modifyProducator("Kaufland");
	assert(item.getProducator() == "Kaufland");
}

void TestItem::testGetItem() {
	Item item("Capsuni", "Fructe", 232.2, "Profi");
	assert(item.getItem() == "Capsuni,Fructe,232.200000,Profi");
}

void TestItem::testIsEqual() {
	Item item("Capsuni", "Fructe", 232.2, "Profi");
	Item item2("Capsuni", "Fructe", 232.2, "Profi");
	Item item3("Capsun", "Fructe", 232.1, "Profi");
	Item item4("Capsuni", "Fructe", 232.2, "Profi");
	assert(item == item2);
	assert((item3 == item4) == false);
}

void TestItem::testAsigned() {
	Item item("Capsuni", "Fructe", 232.2, "Profi");
	Item item2 = item;
	assert(item == item2);
}

