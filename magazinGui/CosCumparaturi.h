#pragma once
#include "Item.h"
#include <vector>
class CosCumparaturi {
private:
	std::vector<Item> cos;
	double totalPret = 0;
public:
	CosCumparaturi() = default;
	~CosCumparaturi() = default;

	void total(double pret);
	void adauga(Item item);
	double getTotal();
	void golireCos();
};

