#pragma once
#include "Repository.h"
#include "Item.h"
#include <string>
#include "FileRepository.h"
#include "Undo.h"
#include <iostream>
#include <vector>
#include <memory>
#include <cstdio>
#include <fstream>
#include <cassert>
#include <functional>

class Service {
private:
	FileRepository<Item> repository;
	std::vector<std::unique_ptr<UndoAction>> undo;
public:
	Service() = default;
	~Service() = default;
	void add(std::string nume, std::string tip, double pret, std::string producator);

	void remove(std::string nume, std::string producator);

	void modifyNume(std::string nume, std::string producator, std::string newNume);

	void modifyTip(std::string nume, std::string producator, std::string newTip);

	void modifyPret(std::string nume, std::string producator, double newPret);

	void modifyProducator(std::string nume, std::string producator, std::string newProducator);
	
	Item getItem(std::string nume, std::string producator);

	std::vector<Item> filtrarePretMaiMic(double pret);
	std::vector<Item> filtrarePretMaiMare(double pret);
	std::vector<Item> filtrareNume(std::string nume);
	std::vector<Item> filtrareProducator(std::string producator);

	std::vector<Item> sortareNume();
	std::vector<Item> sortarePret();
	std::vector<Item> sortareNumeSiTip();

	std::vector<Item> getAll();

	void open() {
		repository.open();
	}

	void doUndo();

	FileRepository<Item> getRepository();
};

class ServiceException
{
	std::string message;

public:
	ServiceException(std::string m) : message{ m } {}
	friend std::ostream &operator<<(std::ostream &out, const ServiceException &ex)
	{
		out << ex.message;
		return out;
	}
};