#pragma once
class TestItem{

public:
	TestItem();
	~TestItem();
	void testConstructor();
	void testAll();

	void testGetNume();
	void testGetTip();
	void testGetPret();
	void testGetProducator();

	void testModifyNume();
	void testModifyTip();
	void testModifyPret();
	void testModifyProducator();

	void testGetItem();

	void testIsEqual();

	void testAsigned();
};

